<?php

function urlCreate($baseurl,$url) {

	


	$url = str_replace(array("&amp;"," "),array("&","%20"),$url);
	if 	($url == "" || $url == null) {
		return $url;
	}

	if (preg_match("/^http:\/\//iU",$url)  || preg_match("/^https:\/\//iU",$url))
	return $url;

	$p = parse_url($baseurl);
	
	if (!isset($p['scheme'])) {
		
			//echo "<br>baseurl=$baseurl<br>url=$url";
			
		
	}
	
	$p['path'] = !isset($p['path'])?"/":$p['path'];
	$pi = pathinfo($p['path']);

	// url starts with / so it's an easy one just attached it to the host
	if (preg_match("/^\//iU",$url)) {
		$url = $p['scheme'].'://'.$p['host'].$url;
	}
	else if (preg_match("/^\.\.\//iU",$url)) {
		// for sites with links like ../

		$b = explode("../",$url);
		$e = explode("/",str_replace("http://","",$baseurl));

		//build the link
		$newURL = "{$p['scheme']}://";
		for ($i=0; $i < count($e)-count($b); $i++) {

			$newURL .= $e[$i]."/";

		}

		$newURL.= $b[count($b)-1];

		$url = $newURL;
	}
	else if (endsWith($baseurl,"\/") && !startsWith($url,"\/")) {

		$url = $baseurl.$url;
	}
	else {


		if (preg_match("/^\?/iU",$url)) {
			$basefile = $p['path'].$url;
		}
		else {
			if ($pi['dirname'] == "\\" || $pi['dirname'] == "/")
			$pi['dirname'] = "";
			$basefile = $pi['dirname']."/".$url;
		}
		$url = $p['scheme'].'://'.$p['host'].preg_replace("/\/.*\/\.\.\//iU","/",$basefile);

	}



	return $url;

}
function get_url_contents($url,$postfields = false,$debug = false,$debugFile = false,$returnHeader=true) {
ini_set('max_execution_time', 30000);
ini_set("memory_limit","100M");

error_reporting(E_ALL);
ini_set('display_errors', 1);

	global $cookie_file_path,$distributionWebsiteId;

	$mydebug = false;
	$referrer = $url;
	
	if ($debug) {
		
	   
		$mydebug = fopen($debugFile,'w');
		fwrite($mydebug,'<META HTTP-EQUIV="Pragma" CONTENT="no-cache"><meta HTTP-EQUIV="cache-control" content="no-cache"> <META HTTP-EQUIV="Expires" CONTENT="Sat Jun 07 00:00:00 1980">'."\n\n");
	}

	//do we need to set a specific referrer url some sites check for it
	if (strpos($url,"|") !== false) {
		$u = explode("|",$url);
		$url = $u[0];
		$referrer = $u[1];
	}
	//echo "url=$url<br/>\npostfield=$postfields<br/>\n";
	$agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 GTBDFff GTB7.0 (.NET CLR 3.5.30729) GTBA";

	//echo $url."<br>";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_USERAGENT, $agent);

	if ($postfields) 
	{
	//	if ($debug) 
	//	{
	//		fwrite($mydebug,"<p>postfields:$postfields</p>");
	//	}

		curl_setopt($ch, CURLOPT_POST, 1);
		//		echo "\n$postfields<br>\n";
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postfields);
	}
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	
	if (!$returnHeader)
		curl_setopt($ch, CURLOPT_HEADER, 0); // Show Headers
	else
		curl_setopt($ch, CURLOPT_HEADER, 1); // Show Headers
	
	
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
	curl_setopt($ch, CURLOPT_REFERER, $referrer);

	
	if ($distributionWebsiteId==94)
		curl_setopt($ch,CURLOPT_ENCODING ,"gzip");
	else
		curl_setopt($ch,CURLOPT_ENCODING ,"gzip,deflate");

	
	if (startsWith($url,'https')) 
	{
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	}

	$headers = array(	"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
						"Accept-Language: en-gb,en;q=0.5",
						"Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
						"Keep-Alive: 300",
						"Connection: keep-alive",
						"Expect:"
					);

	
	
	if (preg_match("/^\{/i",$postfields))
		$headers[] = "Content-Type: text/javascript+json; charset=UTF-8";
		//	Content-Type: multipart/form-data; boundary=---------------------------9010475732170

		
	curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);


	if ($debug) 
	{
		$headerFile = str_replace(array(".html","cache/"),array("_header.html",""),$debugFile);
		$headerOutput = fopen("cache/$headerFile","w");
		fwrite($headerOutput,'<META HTTP-EQUIV="Pragma" CONTENT="no-cache"><meta HTTP-EQUIV="cache-control" content="no-cache"> <META HTTP-EQUIV="Expires" CONTENT="Sat Jun 07 00:00:00 1980">'."\n\n");
		curl_setopt($ch, CURLOPT_STDERR, $headerOutput);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		fwrite($mydebug,'<b>headers sent</b><br><iframe width="100%" height="150" src="'.$headerFile.'" ></iframe>');
	}

	
	if (!$returnHeader)
		$result['content'] = curl_exec($ch);
	else
		$result = curl_redir_exec ($ch,$debug,$mydebug);


	curl_close ($ch);

	if ($debug)
	{
		fclose ($headerOutput);
		fclose ($mydebug);
	}
	
	//sometimes the result content contains javascript redirects will affect our application
	//but sometimes we want to go the same location however then we need to interpret the javascript for if statements
	//so success strings need to come from the page with the javascript that redirects
	$result['content'] = preg_replace(array("/window\.location\.?/i","/location\.replace/i","/location\.href/i"),array("","",""),$result['content']);
	return 	$result;

}


function curl_redir_exec($ch,$debug = false,$mydebug)
{
	static $curl_loops = 0;
	static $curl_max_loops = 5;
	global $stopSubmissionError;

	//	$mydebug = fopen($debugFile,"a");
	//curl_setopt($ch, CURLOPT_TIMEOUT, 120);
	
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	//	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
	//	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


	$data=curl_exec($ch);
	//	echo "\n\n<br><br>*****************\n".htmlspecialchars($data)."\n*****************\n\n<br><br>";

	$connectionInfo = curl_getinfo($ch);
	
	if ($connectionInfo['total_time'] >= 120 && $stopSubmissionError)
	{
		//connection has timedout so lets store that
		global $recordFail,$distributionWebsiteId,$profileId;
		if (isset($recordFail) && $recordFail)
		{
			stopSubmissionError('networkError',$distributionWebsiteId,$profileId);
		}
	}
	else if ($data=='') 
	{
		//		nothing returned
		global $recordFail,$distributionWebsiteId,$profileId;
		if (isset($recordFail) && $recordFail && $stopSubmissionError)
		{
			stopSubmissionError('siteDown',$distributionWebsiteId,$profileId);
		}
	}

	$currentUrlTogether = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
	$current_url = parse_url(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL));

	//	echo "read $currentUrlTogether <br><br>\n\n";

	$l = explode("\r\n\r\n", $data, 2);
	$header = isset($l[0])?$l[0]:'';
	$content = isset($l[1])?$l[1]:'';

	//remove noscript tags to prevent redirects
	global $keepNoScript;
	
	//if (!isset($keepNoScript) || !$keepNoScript)
	//$content = preg_replace("/<noscript>.*<\/noscript>/isU","",$content);

	if ($curl_loops++ >= $curl_max_loops)
	{
		$curl_loops = 0;
		return array("url"=>$currentUrlTogether,"content"=>$content,'header'=>$header);
	}

	//look for any refresh headers
	$headerRefreshRegex = "/Refresh: [0-9]+; URL=(.*)/i";
	preg_match_all($headerRefreshRegex,$content,$headerRefreshMatches);

	//look for any meta refreshes

	$metaRefreshRegex = "/<meta[^>]+http-equiv=[\"']refresh[\"'][^>]+content=[\"'][0-9]+;[^>]*url=([^\"']+)[\"']/i";
	preg_match_all($metaRefreshRegex,$content,$metaRefreshMatches);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	if ($debug)
	{
		fwrite($mydebug,"<br><br>\n\n************************ sent back header *******************<br>\n\nhttp_code=$http_code<br>\n");
		fwrite($mydebug,str_replace("\n","<br>",$header)."<br><br>\n\n");

		//remove any javascript or meta referesh that could make the application redirect
		// "/window.location/i","/location.replace/i","/location.href/i"
		$debugContent = preg_replace(array($metaRefreshRegex,"/<script[^>]*>.*<\/script>/isU","/<iframe[^>]*>.*<\/iframe>/isU"),"",$content);

		//		fwrite($mydebug,"\n------- <b>data send back</b> -----------<br>\n");
		fwrite($mydebug,"<br><br>\n\n************************ data sent back **********************<br>\n".$debugContent."<br><br>\n************************ end data sent back ******************<br><br>\n\n");
		if ($http_code==0 && $stopSubmissionError)
		{
			stopSubmissionError("networkError",$distributionWebsiteId,$profileId);

		}
	}

	//data might also contain header redirect information
	if (preg_match("/HTTP\/1.1 100 Continue/i",$header))
	$header = $content;
	$current_url = $current_url['scheme'] . '://' . $current_url['host'] . (isset($current_url['path'])?$current_url['path']:'') . (isset($current_url['query'])?'?'.$current_url['query']:'');

	//redirect to somewhere else, the URL that the worker puts in should not redirect

	//	echo "httpcode=$http_code<br>\n";

	if ($http_code == 301 || $http_code == 302 || $http_code == 303)
	{

		$matches = array();
		preg_match('/Location:(.*)/i', $header, $matches);
		$redirectURL = str_replace("&#39;","'",trim($matches[1]));
		//		echo "redirecting to : $redirectURL<br>\\n";
		//		return array("url"=>$redirectURL,"content"=>"redirect");

		$redirectURLdata = @parse_url(trim(array_pop($matches)));
		if ($debug)
		{
			fwrite($mydebug,'parse the location url:');
			//			print_r($redirectURLdata);
			fwrite($mydebug,'<br><br>');
		}

		if (!$redirectURLdata)
		{
			//couldn't process the url to redirect to
			$curl_loops = 0;
			return array("url"=>$currentUrlTogether,"content"=>$content,'http_code'=>$http_code);
		}

		//using the current url we are we create the new url as it might not be an absolute url



		if ($debug) {
			fwrite($mydebug," current url:".curl_getinfo($ch, CURLINFO_EFFECTIVE_URL)."<br>redirect url:$redirectURL<br>\n");
			//			print_r($current_url));
			fwrite($mydebug,'<br><br>');
		}


		if (startsWith($redirectURL,'http')  )
		$new_url = $redirectURL;
		else
		$new_url = trim(urlCreate($current_url,$redirectURL));

		if ($debug) {

			fwrite($mydebug,"\n<p>redirecting to 1 $new_url</p>\n");

		}

		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_REFERER, $current_url);
		curl_setopt($ch, CURLOPT_URL,$new_url);
		//		echo '\n\nRedirecting to '. $new_url."\n";
		//		echo "\n--------------------------\n";


		return curl_redir_exec($ch,$debug,$mydebug);
	}
	else if (isset($headerRefreshMatches[1][0]))
	{
		$new_url = $headerRefreshMatches[1][0];
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_REFERER, $current_url);
		$new_url = urlCreate($current_url,$new_url);
		curl_setopt($ch, CURLOPT_URL,$new_url);
		if ($debug)
		{
			fwrite($mydebug,"\n<p>redirecting to 2 $new_url refer from $current_url</p>\n");
		}
		return curl_redir_exec($ch,$debug,$mydebug);

	}
	else if (isset($metaRefreshMatches[1][0]))
	{

		//		$time = time();
		//		$outputFile = "cache/{$websiteKey}_meta_refresh_$time.html";
		//
		//		$fp = fopen($outputFile,"w");
		//		fwrite($fp,print_r($metaRefreshMatches,true));
		//		fclose($fp);


		//sometimes the content will output a meta refresh so we do a redirect to there

		$new_url = str_replace("&#39;","",$metaRefreshMatches[1][0]);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_REFERER, $current_url);
		$new_url = urlCreate($current_url,$new_url);
		curl_setopt($ch, CURLOPT_URL,$new_url);

		if ($debug)
		{

			fwrite($mydebug,"\n<p>redirecting to 2 $new_url refer from $current_url</p>\n");

		}
		return curl_redir_exec($ch,$debug,$mydebug);
	}
	else if (isset($metaRefreshMatches[1][0])) 
	{

		//if the javascript is not within a function that means it's probably a redirect

	}

	else 
	{

		$curl_loops=0;
		//echo $data;
		return array("url"=>$currentUrlTogether,"content"=>$content,'http_code'=>$http_code,'header'=>$header);
	}


}



function endsWith($string,$needle) {

	if (preg_match("/$needle$/i",$string) == 1)
	return true;
	else
	return false;
}

function startsWith($string,$needle) {

	$needle = str_replace(array("\/"),array("/"),$needle); //for old style calls

	$needle = str_replace(array("/","?","[","]","."),array("\/","\?","\[","\]","\."),$needle);

	if (preg_match("/^$needle/i",$string) == 1)
	return true;
	else
	return false;
}


function get_url( $url,  $javascript_loop = 0, $timeout = 5 )
{
    $url = str_replace( "&amp;", "&", urldecode(trim($url)) );

    $cookie = tempnam ("/tmp", "CURLCOOKIE");
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_ENCODING, "" );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
    $content = curl_exec( $ch );
    $response = curl_getinfo( $ch );
    curl_close ( $ch );

    if ($response['http_code'] == 301 || $response['http_code'] == 302)
    {
        ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");

        if ( $headers = get_headers($response['url']) )
        {
            foreach( $headers as $value )
            {
                if ( substr( strtolower($value), 0, 9 ) == "location:" )
                    return get_url( trim( substr( $value, 9, strlen($value) ) ) );
            }
        }
    }

    if (    ( preg_match("/>[[:space:]]+window\.location\.replace\('(.*)'\)/i", $content, $value) || preg_match("/>[[:space:]]+window\.location\=\"(.*)\"/i", $content, $value) ) &&
            $javascript_loop < 5
    )
    {
        return get_url( $value[1], $javascript_loop+1 );
    }
    else
    {
        return array( $content, $response );
    }
}




function getDistance($address,$destination,$mode='driving') {
    
    global $mapsAPIkey;
    
    $address = urlencode($address);
    
    $distancetoPlace = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={$address}&destinations={$destination}&units=metric&key=$mapsAPIkey";
    
    if ($mode=='walking')
        $distancetoPlace .= "&mode=walking";
            
          
          
    $content = get_url_contents($distancetoPlace);
  
    $distance = json_decode($content['content'],true);
   

    $walkingDistanceMeters = isset($distance['rows'][0]['elements'][0]['distance']['value'])? 
       $distance['rows'][0]['elements'][0]['distance']['value']:null;

    $walkingDistanceMinutes = isset($distance['rows'][0]['elements'][0]['duration']['value'])?
    $distance['rows'][0]['elements'][0]['duration']['value']:null;
    
    return array('meters'=>$walkingDistanceMeters,'seconds'=>$walkingDistanceMinutes);
    
}


function getDistanceByPlace($address,$placeID,$mode='driving') {
    
    global $mapsAPIkey;
    
    $address = urlencode($address);
    
    $distancetoPlace = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={$address}&destinations=place_id".urlencode(":")."{$placeID}&units=metric&key=$mapsAPIkey";
    
    if ($mode=='walking')
        $distancetoPlace .= "&mode=walking";
            
          
    $content = get_url_contents($distancetoPlace);
  

    $distance = json_decode($content['content'],true);
   

    $walkingDistanceMeters = isset($distance['rows'][0]['elements'][0]['distance']['value'])?
    $distance['rows'][0]['elements'][0]['distance']['value']:null;

    $walkingDistanceMinutes = isset($distance['rows'][0]['elements'][0]['duration']['value'])?
    $distance['rows'][0]['elements'][0]['duration']['value']:null;
    
    return array('meters'=>$walkingDistanceMeters,'seconds'=>$walkingDistanceMinutes);
    
}

function getCoords($address) {
    
    global $mapsAPIkey;
    
    $address = urlencode($address);
    
     $geocode = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=$mapsAPIkey";
	

          
            $c = get_url_contents($geocode);
            
            $results = json_decode($c['content'],true);
           
			if (!empty($results['results']))
			{
               $latitude = $results['results'][0]['geometry']['location']['lat'];
        
              $longitude = $results['results'][0]['geometry']['location']['lng'];

              return array('latitude'=>$results['results'][0]['geometry']['location']['lat'],'longitude'=>$results['results'][0]['geometry']['location']['lng']);
			}
			else 
				return Array('latitude' => 0,'longitude' => 0);
			
}

function getClosestStations($latitude,$longitude,$radius=5000) {
    
    global $mapsAPIkey;
    
    $closestStationURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$latitude,$longitude&key=$mapsAPIkey&radius={$radius}&type=subway_station|train_station";

	
               
    $content = get_url($closestStationURL);
    
    //$c = get_url_contents($closestStationURL);
    
    $results = json_decode($content[0],true);

    return $results['results'];
}

?>