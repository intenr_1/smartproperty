<?php


include_once('configs.php');
include_once('common.php');
include_once('simple_html_dom.php');

    $DomainURL = array();   
    $vistedURL = array();
    $CurrentURL ;
	$TotalNOPages ;
	$CurrentPage ;
	$urlarray;
    $html;
	
	// html Tag Clases 
	$pageContentBlock = ".new-listing";

	// 2D Array of Html Tag Classes 
	// property type array
	
	$ptype =array(
	'house'=>'duplex,house,semi-detached,terrace,town-house,villa',
	'land'=>'new-land,vacant-land',
	'apartment'=>'apartment-unit-flat,block-of-units,pent-house,studio',
	'rural'=>'acreage-semi-rural,farm,rural',
	'new-devlopments'=>'development-site,new-house-land,new-home-designs,new-apartments'
	);
 
  
	$selectors = array(
		
		'address'=>array("innertext",".listing-result__address-line-1 span"),
		
		'suburb'=>array("innertext",".listing-result__address-line-2 span[itemprop=addressLocality]"), //suburb
		
		'state'=>array("innertext",".listing-result__address-line-2 span[itemprop=addressRegion]"), //state
		
		'postcode'=>array("innertext",".listing-result__address-line-2 span[itemprop=postalCode]"), //postcode
		
		'source_id'=>array("attribute","a[data-listing-id]","data-listing-id"), //source id 
		
		'rooms'=>array("innertext",".listing-result__feature-bed"), //
		
		'bathrooms'=>array("innertext",".listing-result__feature-bathroom"), //
		
		'carparks'=>array("innertext",".listing-result__feature-parking"), //
		
		'price'=>array("innertext",".listing-result__price"), //
		
	);

    
	echo "<pre/>";
	  
	$baseurl='https://www.domain.com.au/';
	$mode='sale';

	$query_to_suburbs= 'select * from suburbs';
	$query_execute=mysql_query($query_to_suburbs);

while ($suburbs = mysql_fetch_assoc($query_execute)) 
{   //1. get data from database .....
	
	$CurrentPage=1;
	$traversingPage=$suburbs['currentPage'];
	$suburb_id=$suburbs['suburb_id'];
	$postcode = $suburbs['postcode'];
    $urb = $suburbs['locality'];
	$state = $suburbs['state'];
    $visited = $suburbs['visited'];
    $ptypeDBValue = $suburbs['ptype'];
	// 2. check if the link  is visited are not  


	if($visited==1)
	{
		continue;
	}
	else
	{   
        isset($ptypeDBValue)?$ptypeDBValue:$ptypeDBValue='house';
	    if(preg_replace('/\s+/', '-', $urb))
	    {
	    	$urb=preg_replace('/\s+/', '-', $urb);
	    }
	   
	    $url= $baseurl.$mode.'/'.$urb.'-'.$state.'-'.$postcode;
	    // 3.2 catagorize the link with property type 
	     foreach ($ptype as $key => $typeValue) 
	     {
	     	 if ($ptypeDBValue != $key and isset($ptypeDBValue))
	     	 {
          		continue;
          	 }
		   
		    $url_que= $url.'/?ptype='.$typeValue.'&ssubs=';
		    
		    $url_query= $url.'/?ptype='.$typeValue.'&ssubs='.$CurrentPage;

		    $html = file_get_html($url_query); 
			$anchor;
	     // 4. get the pagination of the link, total pages to teraverse
			foreach ($html->find('ul.pagination li') as $i=>$li )
			{
				if ($i==0)
				{
					continue;
				}
			
				$anchor = $li->find('a[data-number-of-page]', 0);
			}

			$totalPage= end($anchor->attr);
			$html->clear();
		    $TotalNOPages = $totalPage;
			echo "<br/>"."totalPage = ".$TotalNOPages."<br/>";	
		//  5. get the current page to teraversed 	
			if(!isset($traversingPage))
		    {
		    	($CurrentPage<=1)?$CurrentPage=1:$CurrentPage;
		    }
		    else
		    { // 1. if  in database page = 47 then what 
		      // 2. if   
		    	$CurrentPage=$traversingPage;
		    	if ($traversingPage==$TotalNOPages)
		    	{
	                 $CurrentPage=0;
		    	}

		    }


			for ($page=$CurrentPage; $page <= $TotalNOPages; $page++) 
			{
				if ( $page==1 ) 
				{
					$pageURL  = $url_query;
					echo "<br/>"."pageURL = ".$pageURL."<br/>";	
				}
				else 
				{
					
					$pageURL = $url_que."{$page}";
					echo "<br/>"."pageURL = ".$pageURL."<br/>";
					$update_query="UPDATE suburbs SET currentPage='{$page}',ptype='{$key}' WHERE suburb_id=".$suburb_id;
					if (!mysql_query($update_query)) 
					{	 
						echo mysql_error();
						echo "<br/>"."line 141"."<br/>";
					}
					$ptypeDBValue=NULL;
				} 
				
				// conver page into text 
				$content = get_url_contents($pageURL); 
				$newhtml = str_get_html($content['content']); 

				if (!$newhtml) 
				{  
					$newhtml->clear();
					die("failed");
				}

				foreach ($newhtml->find($pageContentBlock) as $element) 
				{
					$blockhtml = str_get_html($element->innertext);
					$saveData = array();
					$saveData = array('country'=>1);
					
					foreach ($selectors as $name => $selector)
					{  
						$selectorFind = $selector[1];
						$selectorType = $selector[0];
						
					   foreach ($blockhtml->find($selectorFind) as $elements)
					   { 	echo "$selectorFind = ";


							if ($selectorType=='innertext') 
							{
								$value = $elements->innertext;
							}
							else if ($selectorType=='attribute')
							{
								$value =  $elements->attr[$selector[2]];
							}
							
							$value = trim($value);
							$elementHTML = str_get_html($value);

							if ($elementHTML) 
							{
								foreach ($elementHTML->find('span') as $e)
								{
									$e->outertext = '';
								}
								$value = trim($elementHTML->innertext);
								$elementHTML->clear();
							}

							if ($name=='price')
							{
							
								$value = trim(str_ireplace(array("$",",","Offers over "),"",$value));
								//print " trim value = ". $value;
								//echo "<br/>";
								if (preg_match('/[1-9].[1-9]M/', $value))
								{
									$value = intval(preg_replace('/[^0-9]+/', '', $value), 10);
									$value=$value*100000;
								}

								if (preg_match("#[a-z]#",$value))
								{    
									preg_match_all('/([0-9]+)/',$value, $v);
									$value = $v[0];
									
								}
								if(empty($value))
								{
								 if(preg_match("/[0-9]+ ?- ?[0-9]+/", $value,$v))
								 {
									preg_match_all('/([0-9]+)/',$value, $v);
									if(is_bool($v[0][0]))
										{$value = $v[0];}
									else
								   {isset($value) ?$value = $v[0][0]:$value = $v[0] ;}
									
							     }
							    } 
								if (!is_numeric($value))
									$value = 0;
								
								// if price value is not valid 
								if (empty($value) or strlen($value) <4 or strlen($value)> 7)
								{   
									var_dump($value);
									$priceEstimateURL = "https://www.domain.com.au/property-profile/".strtolower(str_replace(array("/"," "),"-",$saveData['address']."-".$saveData['suburb']."-".$saveData['state']."-".$saveData['postcode']));
									echo "priceEstimateURL = ".$priceEstimateURL."<br/>";
									$content = get_url_contents($priceEstimateURL);
									
									//echo $content['content'];
									$estimatehtml = str_get_html($content['content']);
									
									if (isset($estimatehtml) and !is_bool($estimatehtml))
									{
										//echo "finding";
										foreach ($estimatehtml->find("div.proprty-story p") as $elements)
										{
											$priceEstimate = $elements->innertext;
										}
										preg_match('/\$[0-9]+\,[0-9]+,?[0]+/',$priceEstimate,$matches);
										$value = isset($matches[0]) ? $matches[0] : "";
										$value = trim(str_ireplace(array("$",",","Offers over "),"",$value));								
										
									}

									if(isset($estimatehtml) and !is_bool($estimatehtml))
									{		
										$estimatehtml->clear();// die();
									}
									else
									{
										$estimatehtml="";
									}
								}
							
							}
					   }
						if ($name == 'rooms' || $name == 'bathrooms'  || $name == 'carparks' )
						{
					    	(empty($value)) ? $value = 0 : $value;
					    	if ($value == '-')
					    	{
	                           $value=0;
					    	}

						}
						else
						{
						 	(empty($value)) ? $value = NULL : $value;	
						}
						$saveData[$name] = $value;
						echo $value."<br/>";

						$value="";
					}
					            
					$sql = "select source_id , price,ptype from properties where source_id = {$saveData['source_id']}";
					$sqlptype = "update properties set ptype='$key' where source_id = {$saveData['source_id']}";
					$existingProperty = mysql_query($sql);
					$row = mysql_fetch_assoc($existingProperty);
				    if(empty($saveData['ptype']))
				    {
				    	if (!mysql_query($sqlptype)) 
						{ 
							echo mysql_error();
							print "line 262"."<br/>";
						}
				    }
					if (mysql_num_rows($existingProperty)==0)
					{   

						$itemID = $saveData['source_id'];
				         $newPrice =  $saveData['price'] ;
						  
						 $sql = "insert into properties (".implode(",",array_keys($saveData)).") 
						 values
						 (
							'".implode("','",array_values($saveData))."'
						 ) ";
						
						if (!mysql_query($sql)) 
						{ 
							echo mysql_error();
							print $sql." => line 296"."<br/>";
						}
						
						$priceSql = "INSERT INTO Price ( property_id , AddDate , newPrice) VALUES ($itemID,CURDATE(),$newPrice)";
						if (!mysql_query($priceSql)) 
						{ 
							echo mysql_error();
							print $priceSql." => line 303"."<br/>";
						}
					}
					else
					{
						$itemID = $saveData['source_id'];
				        $newPrice =  $saveData['price'] ;
						if ($row['price'] != $newPrice && !empty($newPrice))
						{
					$priceSql = "INSERT INTO Price ( property_id , AddDate , newPrice ) VALUES ($itemID, CURDATE() , $newPrice)";
							if (!mysql_query($priceSql)) 
							{ 
								echo mysql_error();
								print $priceSql." => line 316"."<br/>";
							}
						}				
						
					}
					
					$blockhtml->clear();
					
				}
				
				$newhtml->clear();
				
			}
	    }
		$update_query="UPDATE suburbs SET visited=1 WHERE suburb_id=".$suburb_id;
		if (!mysql_query($update_query)) 
		{ 
			echo mysql_error();
		}
	}	
}

function getPropertyPageData() 
{



}





?>