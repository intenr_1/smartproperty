<?php


include_once('configs.php');
include_once('common.php');
include_once('simple_html_dom.php');

    $DomainURL = array();   
    $vistedURL = array();
    $CurrentURL ;
	$TotalNOPages ;
	$CurrentPage ;
	$urlarray;
    $html;
	
	
	// html Tag Clases 
	$pageContentBlock = ".new-listing";

	// 2D Array of Html Tag Classes 
	$selectors = array(
		
		'address'=>array("innertext",".listing-result__address-line-1 span"),
		
		'suburb'=>array("innertext",".listing-result__address-line-2 span[itemprop=addressLocality]"), //suburb
		
		'state'=>array("innertext",".listing-result__address-line-2 span[itemprop=addressRegion]"), //state
		
		'postcode'=>array("innertext",".listing-result__address-line-2 span[itemprop=postalCode]"), //postcode
		
		'source_id'=>array("attribute","a[data-listing-id]","data-listing-id"), //source id 
		
		'rooms'=>array("innertext",".listing-result__feature-bed"), //
		
		'bathrooms'=>array("innertext",".listing-result__feature-bathroom"), //
		
		'carparks'=>array("innertext",".listing-result__feature-parking"), //
		
		'price'=>array("innertext",".listing-result__price"), //
		
	);
	
	
	// Get the new Url for traversing 
	// Get total no of pages
	/* 1. pick next url in page .
	while all the links in domainurl are not visited get the infomation in linked visited.json  
	if current url has pagination and current page is not equal to final page then 
	go to else part and start fom the currnet page url other wise get a url in domain url and check if it is existed in visited.json then continue else pick a url and gets its pagination and traversing on it and continue untill all the links are visited . */
	


{	
  $pageURL="https://www.domain.com.au/sale/kallaroo-wa-6025/";
  echo "pageURL".$pageURL;
  	// conver page into text 
	$content = get_url_contents($pageURL); 
	$newhtml = str_get_html($content['content']); 
	if (!$newhtml) 
	{  
		$newhtml->clear();
		die("failed");
	}
	
	foreach ($newhtml->find($pageContentBlock) as $element) 
	{    
		$blockhtml = str_get_html($element->innertext);
		$saveData = array();
		$saveData = array('country'=>1);
		
		foreach ($selectors as $name => $selector)
		{  
			$selectorFind = $selector[1];
			$selectorType = $selector[0];

		   foreach ($blockhtml->find($selectorFind) as $elements)
		   {
		   	    
				if ($selectorType=='innertext') 
				{
					$value = $elements->innertext;
				}
				else if ($selectorType=='attribute')
				{
					$value =  $elements->attr[$selector[2]];
				}
				
				$value = trim($value);

				$elementHTML = str_get_html($value);
				if ($elementHTML) 
				{
					foreach ($elementHTML->find('span') as $e)
					{
						$e->outertext = '';
					}
					$value = trim($elementHTML->innertext);
					$elementHTML->clear();
				}
				if ($name=='price')
				{
					
					echo "<br/>";
					echo "price=".$value;
					echo "<br/>";
					$value = trim(str_ireplace(array("$",",","Offers over "),"",$value));
					//print " trim value = ". $value;
					//echo "<br/>";
					if (preg_match('/[1-9].[1-9]M/', $value))
					{
						$value = intval(preg_replace('/[^0-9]+/', '', $value), 10);
						$value=$value*100000;
					}

					if (preg_match("#[a-z]#",$value))
					{    
						preg_match_all('/([0-9]+)/',$value, $v);
						$value = $v[0][0];
						isset($value) ?$value = $v[0][0]:$value = $v[0] ;
						//echo "value = ".$value;
						//echo "<br/>";
					}
					else if(preg_match("/[0-9]+ ?- ?[0-9]+/", $value))
					{
						preg_match_all('/([0-9]+)/',$value, $v);
						$value = $v[0][0];
						isset($value) ?$value = $v[0][0]:$value = $v[0] ;
						//echo "value = ".$value;
						
						// echo "<br/>";
						// $value = intval(preg_replace('/[^0-9]+/', '', $value), 10);
						// print "line 115 = ". $value;
						// echo "<br/>";
				    } 
					if (!is_numeric($value))
						$value = 0;
					
					// if price value is not valid 
					if (empty($value) or strlen($value) <4 or strlen($value)> 7)
					{   
						echo "<pre/>";
						// var_dump($saveData);
						// echo "address =".$saveData['address']."<br/>";
						// echo "address =".$saveData['suburb']."<br/>";
						// echo "address =".$saveData['state']."<br/>";
						// echo "address =".$saveData['postcode']."<br/>";
						
						//try to get price from the estimate page 
						$priceEstimateURL = "https://www.domain.com.au/property-profile/".strtolower(str_replace(array("/"," "),"-",$saveData['address']."-".$saveData['suburb']."-".$saveData['state']."-".$saveData['postcode']));
						echo "priceEstimateURL = ".$priceEstimateURL."<br/>";
						$content = get_url_contents($priceEstimateURL);
						
						//echo $content['content'];
						$estimatehtml = str_get_html($content['content']);
						
						if ($estimatehtml)
						{
							//echo "finding";
							foreach ($estimatehtml->find("div.proprty-story p") as $elements)
							{
								$priceEstimate = $elements->innertext;
							}
							preg_match('/\$[0-9]+\,[0-9]+,?[0]+/',$priceEstimate,$matches);
							$value = isset($matches[0]) ? $matches[0] : "";
							$value = trim(str_ireplace(array("$",",","Offers over "),"",$value));								
							
						}

						print "value = ".$value;
						echo "<br/>";
						echo "<--------------------------------------------------->";
						echo "<br/>";
						
	
                     						
						$estimatehtml->clear();// die();
						
					}
					
					print "value = ".$value;
						echo "<br/>";
						echo "<--------------------------------------------------->";
						echo "<br/>";
				}
			
			}
			
		     (empty($value)) ? $value = "null" : $value;
			 $saveData[$name] = $value;
			 $value="";
			//$value="";
			
		}
		            
		
		
		$blockhtml->clear();
			
	}
	
	$newhtml->clear();
	
	
	
}


function getPropertyPageData() 
{



}


/*
{
							//sometimes the price is not available
							$value = trim(str_ireplace(array("$",",","Offers over "),"",$value));
							 
							if (preg_match("#[a-z]#",$value))
							{
								preg_match_all('/([0-9]{3}+)/',$value, $v);
								$value = $v[1];
							
							}
							
							$value = intval(preg_replace('/[^0-9]+/', '', $value), 10);
							if (!is_numeric($value))
								$value = 0;
							
							// if price value is not valid 
							if (empty($value) or strlen($value) <2 or strlen($value)> 7)
							{   
								//try to get price from the estimate page 
								$priceEstimateURL = "https://www.domain.com.au/property-profile/".strtolower(str_replace(array("/"," "),"-",$saveData['address']."-".$saveData['suburb']."-".$saveData['state']."-".$saveData['postcode']));
								//echo "priceEstimateURL = ".$priceEstimateURL."<br/>";
								$content = get_url_contents($priceEstimateURL);
								
								//echo $content['content'];
								$estimatehtml = str_get_html($content['content']);
								
								if ($estimatehtml)
								{
									//echo "finding";
									foreach ($estimatehtml->find("div.proprty-story p") as $elements)
									{
										$priceEstimate = $elements->innertext;
									}
									preg_match('/\$[0-9]+\,[0-9]+/',$priceEstimate,$matches);
									$value = isset($matches[0]) ? $matches[0] : "";
									$value = trim(str_ireplace(array("$",",","Offers over "),"",$value));								
									
								}
								
								$estimatehtml->clear();// die();
								
							}
							
							
						}
*/

?>